const server = require('./src');
const functions = require('firebase-functions');
exports.app = functions.https.onRequest(server.app);