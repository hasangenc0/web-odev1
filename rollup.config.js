/* eslint-disable */
import { terser } from "rollup-plugin-terser";
import postcss from "rollup-plugin-postcss";

function buildCss({ prerender, watch } = {}) {
  return {
    input: "src/scss/cv.css",
    output: {
      file: "public/js/cv/inject_styles.js",
      format: 'iife'
    },
    watch: { clearScreen: false },
    plugins: [
      postcss(),
      terser(),
    ].filter(item => item),
  };
}

function buildJs({ prerender, watch } = {}) {
  return {
    input: "src/client/cv.js",
    output: {
      file: "public/js/cv/cv.js",
      format: 'iife'
    },
    watch: { clearScreen: false },
    plugins: [
      terser(),
    ].filter(item => item),
  };
}

export default function ({watch}) {
    return [buildCss({watch, prerender: false}), buildJs({watch, prerender: false})];
}