module.exports = {
  theme: {
    extend: {
      colors: {
        gbl: '#1d1e1f',
        lbl: '#94ceff'
      },
      width: {
        sidebar: '220px',
        content: '500px'
      }
    }
  },
  variants: {},
  plugins: [],
}
