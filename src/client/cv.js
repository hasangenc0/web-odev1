function hideContents() {
  var contents = document.querySelectorAll('.content article');
  for (let j = 0; j < contents.length; j++) {
    contents[j].classList.add('hidden');
  }
}


function init() {
  var links = document.querySelectorAll('[data-content]');
  for (let i = 0; i < links.length; i++) {
    links[i].addEventListener("click", function() {
      hideContents();
      var contentId = links[i].dataset.content;
      var content = document.querySelector(contentId);
      if (content) {
        content.classList.remove('hidden');
        history.replaceState(contentId, contentId, contentId);
      }
    });
  }
}

if (history.state && history.state.length > 0) {
  hideContents();
  var content = document.querySelector(history.state);
  if (content) {
    content.classList.remove('hidden');
  }
}

init();
performance.mark("ui is ready");
