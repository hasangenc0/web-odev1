const path = require('path');
const functions = require('firebase-functions');
const express = require('express');

const app = express();

// set the view engine to ejs
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));
app.get('/' , (req,res) => {
    res.render('pages/index');
});

app.get('/cv' , (req,res) => {
    res.render('cv');
});

app.get('/me' , (req,res) => {
    res.render('cv');
});

exports.app = functions.https.onRequest(app);
